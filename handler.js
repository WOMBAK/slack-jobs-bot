const axios = require("axios");
const {
  parseDate,
  postedYesterdayJob,
  frontendJob,
  notDesignerJob,
  backendJob,
  logJob,
  and,
  or,
} = require("./lib/utils");

const { getJobsFromRss } = require("./lib/rss");
const crawler = require("./lib/crawler");

const getDomestikaJobs = () =>
  crawler({
    url: "https://www.domestika.org/es/jobs/where/remote",
    selector: ".job-item:not(.job-item--banner)",
    fields: [
      {
        name: "title",
        selector: ".job-item__title",
      },
      {
        name: "title_link",
        selector: ".job-title",
        attr: "href",
      },
      {
        name: "date",
        selector: ".job-item__date",
      },
      {
        name: "text",
        selector: ".job-item__excerpt",
      },
    ],
  }).then(jobs =>
    jobs.map(job => Object.assign({}, job, { date: parseDate(job.date) }))
  );

const getBetabeersJobs = () =>
  crawler({
    url: "https://betabeers.com/post/",
    selector: ".joblist li",
    fields: [
      {
        name: "title",
        selector: ".title",
      },
      {
        name: "title_link",
        selector: ".title",
        attr: "href",
      },
      {
        name: "date",
        selector: ".extrainfo",
      },
      {
        name: "text",
        selector: "*",
        regex: ".*$",
      },
    ],
  }).then(jobs =>
    jobs
      .filter(job => /remote|remoto|deslocalizado/.test(job.text))
      .map(job => Object.assign({}, job, { date: parseDate(job.date) }))
  );

const getJobs = () =>
  Promise.all([
    getDomestikaJobs(),
    getBetabeersJobs(),
    getJobsFromRss("https://stackoverflow.com/jobs/feed?l=Remote&u=Km&d=20"),
  ]).then(([domestikaJobs, betabeersJobs, stackOverflowJobs]) => [
    ...domestikaJobs,
    ...betabeersJobs,
    ...stackOverflowJobs,
  ]);

// formatSlackMessage : Job -> SlackMessage
const formatSlackMessage = ({ title, text, title_link, fields = [] }) => ({
  text: "New project found!",
  attachments: [
    {
      title,
      text,
      title_link,
      fields,
    },
  ],
});

// send the message to the slack webhook
const sendBotMessage = message =>
  axios.post(process.env.SLACK_WEBHOOK, message).catch(console.log);

module.exports.scrape = () => {
  console.log(`Executing task @ ${new Date()}`);

  getJobs().then(jobs =>
    jobs
      .filter(
        and(postedYesterdayJob, or(frontendJob, backendJob), notDesignerJob)
      )
      .map(logJob)
      .map(formatSlackMessage)
      .forEach(sendBotMessage)
  );
};
