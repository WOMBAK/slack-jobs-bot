const axios = require('axios');
const cheerio = require('cheerio');

/*
type Field = {
  name: string,
  selector: string,
  attr?: string,
  regex?: string
}
*/

const extractItem = (fields, $item) =>
  fields.reduce((item, {name, selector, attr, regex}) => {
    const value = [$item.find(selector)]
      .map(v => (attr ? v.attr(attr) : v.text()))
      .map(v => (regex ? v.match(new RegExp(regex)).shift() : v))
      .pop();

    return Object.assign(item, {[name]: value});
  }, {});

module.exports = ({url, selector, fields}) =>
  axios
    .get(url)
    .then(response => {
      const $ = cheerio.load(response.data);

      const $items = $(selector);

      const items = $items.map((_, el) => extractItem(fields, $(el))).get();

      return items;
    })
    .catch(err => {
      console.error(`Error retrieving items from ${url}`, err);
      return Promise.resolve([]);
    });
