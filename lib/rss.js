'use strict';

const { parseURL } = require('rss-parser');

const EXCERPT_LENGTH = 200;

const formatText = (text = '') => `${text.substring(0, EXCERPT_LENGTH)}...`;

const categoriesField = (categories = []) => ({
  title: "Categories",
  value: categories.join(", "),
  short: false,
});

// Giving a rss entry returns a job object
// rssEntryToJob : Rss entry -> Job
const rssEntryToJob = ({title, link, isoDate, categories = [], contentSnippet}) => ({
  title,
  title_link: link,
  date: new Date(isoDate),
  text: formatText(contentSnippet),
  fields: [categoriesField(categories)],
});

const getJobsFromRss = (url) => new Promise((resolve) => {
  parseURL(url, (err, {feed} ) => {
    if(err){
      console.error(`Error while retrieving jobs from ${url}`, err);
      return resolve([]);
    }

    if(!feed || !feed.entries){
      return resolve([]);
    }

    try{
      const jobs = feed.entries.map(e => rssEntryToJob(e));
      resolve(jobs);
    }catch(e){
      console.error(`Error while parsing jobs from ${url}`, err);
      resolve([]);
    }
  })
});

module.exports = {
  getJobsFromRss
};
