// and : List a -> b -> Bool
const and = (...args) => el => args.every(f => f(el));
// or : List a -> b -> Bool
const or = (...args) => el => args.some(f => f(el));

// Backend : Job -> Bool
const backendJob = job => /backend|back end/i.test(job.title);

// frontendJob : Job -> Bool
const frontendJob = job => /web|frontend|front end/i.test(job.title);

// javascriptJob : Job -> Bool
const javascriptJob = job => /javascript/i.test(job.title);

// wordpressJob : Job -> Bool
const wordpressJob = job => /wordpress/i.test(job.title);

// notDesignerJob : Job -> Bool
const notDesignerJob = job => !/diseñador|designer/i.test(job.title);

// logJob -> Job -> Job
const logJob = job => console.log(`Found job: ${job.title}`) || job;

// Givin a string with the format dd/mm/yy or dd/mm/yyy returns a Date
// parseDate : String -> Date
const parseDate = str =>
  new Date(str.replace(/(\d{2})\/(\d{2})\/(\d{2,4})/, '$2/$1/$3'));

// postedTodayJob : Job -> Bool
const postedTodayJob = job => today(job.date);

// postedYesterdayJob : Job -> Bool
const postedYesterdayJob = job => yesterday(job.date);

// Check if two dates are in the same day
// sameDay : Date -> Date -> Bool
const sameDay = date1 => date2 =>
  date1.getDate() === date2.getDate() &&
  date1.getMonth() === date2.getMonth() &&
  date1.getFullYear() === date2.getFullYear();

const today = date => sameDay(date)(new Date(Date.now()));

const yesterday = date =>
  sameDay(date)(new Date(Date.now() - 24 * 60 * 60 * 1000));

module.exports = {
  and,
  or,
  backendJob,
  frontendJob,
  javascriptJob,
  wordpressJob,
  notDesignerJob,
  logJob,
  parseDate,
  postedTodayJob,
  postedYesterdayJob,
  sameDay,
  today,
  yesterday,
};
